const userData = {
  name: "ABC",
  email: "abc@gmail.com",
  age: 30,
}

const addressData = {
  city: "Ahmedabad",
  state: "Gujarat",
  pincode: "383748"
}

export const homeResponses = {
  userData,
  addressData
}