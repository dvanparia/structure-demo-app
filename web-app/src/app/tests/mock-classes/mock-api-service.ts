import { of } from 'rxjs';
import { homeResponses } from '../fixtures';

export class MockApiService {
  getUserData() {
    return of(homeResponses.userData);
  }

  getAddressData() {
    return of(homeResponses.addressData);
  }
}