export interface Bank {
    id?: string;
    code?: string;
    name?: string;
    status?: string;
}