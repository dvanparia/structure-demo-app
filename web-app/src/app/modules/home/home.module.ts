import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardComponent } from './components';
import { HomeComponent } from './components/home.component';
import { HomeRoutingModule } from './home-routing.module';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { AddBankComponent } from './components/add-bank/add-bank.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CoreModule,
    RouterModule,
    SharedModule,
    CommonModule,
    HomeRoutingModule,
    TableModule,
    InputTextModule,
    ToolbarModule,
    ButtonModule,
    RippleModule,
    FormsModule
  ],
  declarations: [
    HomeComponent,
    DashboardComponent,
    AddBankComponent,
  ],
  exports: [
  ]
})
export class HomeModule { }