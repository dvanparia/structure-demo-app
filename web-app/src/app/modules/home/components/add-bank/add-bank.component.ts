import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BankService } from 'src/app/core';
import { Bank } from 'src/app/shared';

@Component({
  selector: 'app-add-bank',
  templateUrl: './add-bank.component.html',
  styleUrls: ['./add-bank.component.scss']
})
export class AddBankComponent implements OnInit {

  bank: Bank = {
    id : '',
    code : '',
    name : '',
    status : ''
  };

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(form) {
    this.router.navigate(['/home/']);
  }

  onCancel() {
    this.router.navigate(['/home/']);
  }

}
