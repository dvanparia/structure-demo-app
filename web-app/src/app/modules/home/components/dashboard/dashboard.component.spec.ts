import { ComponentFixture, TestBed } from '@angular/core/testing';
import { homeResponses } from 'src/app/tests/fixtures';

import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#setData() should set proper user data', () => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    component.setData(homeResponses.userData);
    expect(component.user).toEqual(homeResponses.userData);
  });
});
