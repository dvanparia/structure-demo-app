import { Component, OnInit, ViewChild } from '@angular/core';
import { Bank } from 'src/app/shared/interfaces/bank';
import {Table} from 'primeng/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  styles: [`
        @media screen and (max-width: 960px) {
          :host ::ng-deep .p-datatable.p-datatable-customers.rowexpand-table .p-datatable-tbody > tr > td:nth-child(6) {
            display: flex;
          }
        }`
      ],
})
export class DashboardComponent implements OnInit {

  user = { }

  banks: Bank[] = [];

  selectedBanks: Bank[] = [];

  @ViewChild('dt')
  table!: Table;
  
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.banks = [
      {
        id: '1',
        code: 'BNS',
        name: 'Bank of Nova Scotia, Jamaica',
        status: 'INACTIVE',
      },
      {
        id: '2',
        code: 'FCIB',
        name: 'First Caribbean International',
        status: 'ACTIVE',
      },
      {
        id: '3',
        code: 'NCB',
        name: 'National Commercial Bank',
        status: 'ACTIVE',
      },
      {
        id: '4',
        code: 'RBTT',
        name: 'Royal Bank of Trinidad & Tobago',
        status: 'ACTIVE',
      },
    ]
  }

  setData(user: any) {
    this.user = user
  }

  onAdd() {
    this.router.navigate(['/home/add']);
  }

}
