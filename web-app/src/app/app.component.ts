import { Renderer2 } from '@angular/core';
import {Component, OnInit} from '@angular/core';
import {PrimeNGConfig} from 'primeng/api';
import { MenuService } from './core/services/menu/menu.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit{

    layoutMode = 'static';

    darkMenu = false;

    profileMode = 'top';

    ripple = true;

    inputStyle = 'outlined';

    rotateMenuButton!: boolean;

    topbarMenuActive!: boolean;

    overlayMenuActive!: boolean;

    staticMenuDesktopInactive!: boolean;

    staticMenuMobileActive!: boolean;

    layoutMenuScroller!: HTMLDivElement;

    menuClick!: boolean;

    topbarItemClick!: boolean;

    activeTopbarItem: any;

    menuHoverActive!: boolean;

    configActive!: boolean;

    configClick!: boolean;

    constructor(public renderer: Renderer2, private menuService: MenuService,
                private primengConfig: PrimeNGConfig) {}

    onLayoutClick() {
        if (!this.topbarItemClick) {
            this.activeTopbarItem = null;
            this.topbarMenuActive = false;
        }

        if (!this.menuClick) {
            if (this.isHorizontal() || this.isSlim()) {
                this.menuService.reset();
            }

            if (this.overlayMenuActive || this.staticMenuMobileActive) {
                this.hideOverlayMenu();
            }

            this.menuHoverActive = false;
        }

        if (this.configActive && !this.configClick) {
            this.configActive = false;
        }

        this.configClick = false;
        this.topbarItemClick = false;
        this.menuClick = false;
    }

    onMenuButtonClick(event: { preventDefault: () => void; }) {
        this.menuClick = true;
        this.rotateMenuButton = !this.rotateMenuButton;
        this.topbarMenuActive = false;

        if (this.layoutMode === 'overlay') {
            this.overlayMenuActive = !this.overlayMenuActive;
        } else {
            if (this.isDesktop()) {
                this.staticMenuDesktopInactive = !this.staticMenuDesktopInactive; } else {
                this.staticMenuMobileActive = !this.staticMenuMobileActive; }
        }

        event.preventDefault();
    }

    onMenuClick($event: any) {
        this.menuClick = true;
    }

    onTopbarMenuButtonClick(event: { preventDefault: () => void; }) {
        this.topbarItemClick = true;
        this.topbarMenuActive = !this.topbarMenuActive;

        this.hideOverlayMenu();

        event.preventDefault();
    }

    onTopbarItemClick(event: { preventDefault: () => void; }, item: any) {
        this.topbarItemClick = true;

        if (this.activeTopbarItem === item) {
            this.activeTopbarItem = null; } else {
            this.activeTopbarItem = item; }

        event.preventDefault();
    }

    onTopbarSubItemClick(event: { preventDefault: () => void; }) {
        event.preventDefault();
    }

    onConfigClick(event: any) {
        this.configClick = true;
    }

    onRippleChange(event: any) {
        this.ripple = event.checked;
        this.primengConfig = event.checked;
    }

    hideOverlayMenu() {
        this.rotateMenuButton = false;
        this.overlayMenuActive = false;
        this.staticMenuMobileActive = false;
    }

    isTablet() {
        const width = window.innerWidth;
        return width <= 1024 && width > 640;
    }

    isDesktop() {
        return window.innerWidth > 1024;
    }

    isMobile() {
        return window.innerWidth <= 640;
    }

    isStatic() {
        return this.layoutMode === 'static';
    }

    isOverlay() {
        return this.layoutMode === 'overlay';
    }

    isHorizontal() {
        return this.layoutMode === 'horizontal';
    }

    isSlim() {
        return this.layoutMode === 'slim';
    }

    ngOnInit() {
        this.primengConfig.ripple = true;
    }
}

