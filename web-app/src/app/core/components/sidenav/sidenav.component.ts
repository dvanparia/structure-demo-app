import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
})
export class SidenavComponent implements OnInit {

  model: any[] = [];

  ngOnInit() {
    this.model = [
      {
        label: 'Master Screen',
        items: [
          {
            label: 'Association/Affiliation', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'Bank', icon: 'pi pi-fw pi-money-bill', routerLink: ['/home/listing'],
          },
          {
            label: 'Customer', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'Fee Management', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'ID Type', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'Insurance Management', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'Licence Parameters', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'Office Management', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'Receipt Message', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'Signatory', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'System Configuration', icon: 'pi pi-fw pi-home',
          },
          {
            label: 'Timing Point', icon: 'pi pi-fw pi-home',
          },
        ]
      },
    ];
  }

}
