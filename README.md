# Ionic and Angular Demo Folder Structure Application

This application is purely a folder structure demo for the Ionic Framework and Angular.

## Table of Contents
- [Getting Started](#getting-started)
- [Running](#running)
  - [Web App](#web-app)
  - [Android & IOS](#android-&-ios)

## Getting Started

* [Download the installer](https://nodejs.org/) for Node LTS.
* Install the angular CLI globally: `npm install -g @angular/cli`
* Install the ionic CLI globally: `npm install -g ionic`
* Clone this repository: `git clone https://gitlab.com/dvanparia/structure-demo-app.git`.

## Running

### Web App

1. Open web-app directory
2. Run `npm install` to install the required dependencies
3. Run `npm start` or `ng serve` to run the app on dev server
4. Run `npm build` or `ng build` to build the project or `ng build --prod` to build in production mode

### Android & IOS

1. Open mobile-app directory
2. Run `npm install` to install the required dependencies
3. Run `ionic build` to build the project
4. Add android and ios platforms using
    * `npx cap add android`
    * `npx cap add ios`
5. Open respective IDEs to open the generated projects
    * `npx cap open android`
    * `npx cap open ios`
6. Configure "linuxAndroidStudioPath" property in capacitor.config.json if there is any issue   in step 4
7. Everytime you build your project
    * Run `npx cap copy` to sync your projects

