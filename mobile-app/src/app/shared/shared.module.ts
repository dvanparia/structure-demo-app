import { NgModule } from '@angular/core';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { HighlightDirective } from './directives/highlight/highlight.directive';
import { FilterPipe } from './pipes/filter/filter.pipe';

@NgModule({
  imports: [],
  declarations: [
    LoadingSpinnerComponent,
    HighlightDirective,
    FilterPipe
  ],
  exports: []
})
export class SharedModule { }