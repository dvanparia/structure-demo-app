import { NgModule } from '@angular/core';
import { HeaderComponent } from './components/header/header.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';

@NgModule({
  imports: [
  ],
  declarations: [
    HeaderComponent,
    SidenavComponent
  ],
  exports: [
  ]
})
export class CoreModule { }